import "./App.css";
import Table1 from "./component/Table1";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useState } from "react";
import { useCallback } from "react";

function App() {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();
  const resetAsyncForm = useCallback(async () => {
    const result = await fetch("./api/formValues.json");
    reset(result);
  }, [reset]);

  const [inputList, setInputList] = useState([]);
  const onSubmit = (data) => console.log(data);

  console.log(errors);

  const Input = () => {
    return (
      <div className="seatDesc">
        <div>
          <label>К-сть</label>
          <input
            {...register(
              "amount",
              { required: true },
              {
                validate: (value) => value.length > 0,
              }
            )}
          ></input>
          {errors.amount && <p>Невірне число</p>}
        </div>
        <div>
          <label>Вартість</label>
          <input
            {...register(
              "cost",
              { required: true },
              {
                validate: (value) => value.length > 0,
              }
            )}
          ></input>
          {errors.cost && <p>Невірне число</p>}
        </div>
        <div>
          <label>Вага</label>
          <input
            {...register(
              "weight",
              { required: true },
              {
                validate: (value) => value.length > 0,
              }
            )}
          ></input>
          {errors.weight && <p>Невірне число</p>}
        </div>
        <div>
          <label>Довжина</label>
          <input
            {...register(
              "lenght",
              { required: true },
              {
                validate: (value) => value.length > 0,
              }
            )}
          ></input>
          {errors.lenght && <p>Невірне число</p>}
        </div>
        <div>
          <label>Ширина</label>
          <input
            {...register(
              "width",
              { required: true },
              {
                validate: (value) => value.length > 0,
              }
            )}
          ></input>
          {errors.width && <p>Невірне число</p>}
        </div>
        <div>
          <label>Висота</label>
          <input
            {...register(
              "height",
              { required: true },
              {
                validate: (value) => value.length > 0,
              }
            )}
          ></input>
          {errors.height && <p>Невірне число</p>}
        </div>
        <br></br>
      </div>
    );
  };

  const onAddBtnClick = (event) => {
    setInputList(inputList.concat(<Input key={inputList.length} />));
  };

  const [isInputShown, setIsInputShown] = useState(false);
  const checkboxHandler = () => {
    setIsInputShown(!isInputShown);
  };

  return (
    <React.Fragment>
      <Table1></Table1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <section>
          <div>
            <p>Маршрут</p>
          </div>
          <div>
            <label>Місто відправник</label>
            <select {...register("citySender", { required: true })}>
              <option value="Київ">Київ</option>
              <option value="Житомир">Житомир</option>
              <option value="Вінниця">Вінниця</option>
              <option value="Дніпро">Дніпро</option>
            </select>
          </div>
          <div>
            <label>Місто отримувач</label>
            <select {...register("cityRecipient", { required: true })}>
              <option value="Київ">Київ</option>
              <option value="Житомир">Житомир</option>
              <option value="Вінниця">Вінниця</option>
              <option value="Дніпро">Дніпро</option>
            </select>
          </div>
          <br></br>
        </section>

        <hr></hr>

        <section>
          <label className="inline">Вид Відправлення</label>
          <select
            className="inline"
            {...register("shipmentType", { required: true })}
          >
            <option value="Вантажі">Вантажі</option>
            <option value="Палети">Палети</option>
          </select>
        </section>

        <hr></hr>

        <section>
          <div>
            <label>Характеристика місць</label>
            <div className="seatDesc">
              <div>
                <label>К-сть</label>
                <input
                  type="number"
                  {...register(
                    "amount",
                    { required: true },
                    {
                      validate: (value) => value.length > 0,
                    }
                  )}
                ></input>
                {errors.amount && <p>Невірне число</p>}
              </div>
              <div>
                <label>Вартість</label>
                <input
                  type="number"
                  {...register(
                    "cost",
                    { required: true },
                    {
                      validate: (value) => value.length > 0,
                    }
                  )}
                ></input>
                {errors.number && <p>Невірне число</p>}
              </div>
              <div>
                <label>Вага</label>
                <input
                  type="number"
                  {...register(
                    "weight",
                    { required: true },
                    {
                      validate: (value) => value.length > 0,
                    }
                  )}
                ></input>
                {errors.weight && <p>Невірне число</p>}
              </div>
              <div>
                <label>Довжина</label>
                <input
                  type="number"
                  {...register(
                    "lenght",
                    { required: true },
                    {
                      validate: (value) => value.length > 0,
                    }
                  )}
                ></input>
                {errors.lenght && <p>Невірне число</p>}
              </div>
              <div>
                <label>Ширина</label>
                <input
                  type="number"
                  {...register(
                    "width",
                    { required: true },
                    {
                      validate: (value) => value.length > 0,
                    }
                  )}
                ></input>
                {errors.width && <p>Невірне число</p>}
              </div>
              <div>
                <label>Висота</label>
                <input
                  type="number"
                  {...register(
                    "height",
                    { required: true },
                    {
                      validate: (value) => value.length > 0,
                    }
                  )}
                ></input>
                {errors.height && <p>Невірне число</p>}
              </div>
              <br></br>
              <div>
                <button className="btn" onClick={onAddBtnClick}>
                  <u>Додати місце</u>
                </button>
                {inputList}
              </div>
            </div>
          </div>
        </section>

        <br></br>
        <hr></hr>

        <section>
          <div>
            <label>Послуга "Пакування"</label>
          </div>
          <div>
            <input
              type="checkbox"
              className="marginLR"
              {...register("pacakging")}
            ></input>
            <button className="customBtn">Тарифи пакування</button>
          </div>

          <br></br>
          <div>
            <label>Послуга "Підйом на поверх"</label>
          </div>
          <div>
            <input
              type="number"
              className="marginLR"
              {...register("upstairs")}
            ></input>
          </div>
          <div>
            <label className="negMargin">Кількість поверхів</label>
          </div>
          <div>
            <label className="negMargin">
              Ліфт <input type="checkbox" {...register("elevator")}></input>
            </label>
          </div>
          <br></br>
          <div>
            <label>
              Послуга "Зворотня доставка"
              <input
                type="checkbox"
                checked={isInputShown}
                onChange={checkboxHandler}
                {...register("returnDeliveryService")}
              />
            </label>
            <div>
              <label>Вид зворотньої доставки</label>
            </div>
            {isInputShown && (
              <div>
                <select
                  className="marginLR"
                  {...register("returnDeliveryServiceType")}
                >
                  <option value="documents">Документи</option>
                  <option value="remittance">Грошовий переказ</option>
                </select>
              </div>
            )}
          </div>
        </section>
        <br></br>
        <div>
          <input type="submit" value={"Розрахувати"}></input>
          <input className="btn" type="reset" value="Очистити"></input>
        </div>
      </form>
    </React.Fragment>
  );
}

export default App;
