import React, { useState, useEffect } from "react";
import {
  List,
  AutoSizer,
  CellMeasurer,
  CellMeasurerCache,
} from "react-virtualized";

import "./App.css";
import data from "./data";


const posts = data.filter((data) => {
  return data.title.split(" ").length > 7;
});

export default function App() {
  const cache = React.useRef(
    new CellMeasurerCache({
      fixedWidth: true,
      defaultHeight: 100,
    })
  );

  return (
    <div>
      <div style={{ width: "100%", height: "100vh" }}>
        <AutoSizer>
          {({ width, height }) => (
            <List
              width={width}
              height={height}
              rowHeight={cache.current.rowHeight}
              deferredMeasurementCache={cache.current}
              rowCount={posts.length}
              rowRenderer={({ key, index, style, parent }) => {
                const data = posts[index];

                return (
                  <CellMeasurer
                    key={key}
                    cache={cache.current}
                    parent={parent}
                    columnIndex={0}
                    rowIndex={index}
                  >
                    <div style={style}>
                      <p>{data.title}</p>
                      <Image
                      url={data.url}
                      thumbnailUrl={data.thumbnailUrl}
                      >
                      </Image>
                    </div>
                  </CellMeasurer>
                );
              }}
            />
          )}
        </AutoSizer>
      </div>
    </div>
  );
}
function Image(props) {
  return (
    <div>
      <a href={props.url} target="_blank" rel="noreferrer">
        <p>{props.thumbnailUrl}</p>
      </a>
    </div>
  );
}