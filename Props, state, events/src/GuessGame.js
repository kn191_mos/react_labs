import { useEffect } from "react";
import { useState } from "react";
import Button from '@mui/material/Button';

export default function GuessGame() {
  const [count, setCount] = useState(0);
  const [secretNum, setNum] = useState(0);
  const [gameState, setGameState] = useState(true);
  const [newGame, setNewGame] = useState(false);
  const [userNum, setUserNum] = useState(0);
  const [result, setResult] = useState("");

  const countRestrict = 10;

  useEffect(() => {
    if (newGame) {
      setGameState(false);
      console.log(userNum);
      console.log(secretNum);
    }
  });

  const HandleNewGame = () => {
    setNum(Math.floor(Math.random() * (1000 - 1 + 1)) + 1);
  };

  const HandleGameProcess = () => {
    setCount(count + 1);
    if (userNum == secretNum || count == countRestrict -1) {
      setNewGame(false);
      setGameState(true);
    }
  };

  const Result = () => {
    const userValue = userNum;
    if (userValue == secretNum) setResult(<h3>Correct!</h3>);
    else if (secretNum > userNum) setResult(<p>N is more than {userNum} </p>);
    else if (secretNum < userNum) setResult(<p>N is less than {userNum}</p>);
    if (count == countRestrict - 1) setResult(<h3>You lost!</h3>)
  };

  return (
    <div>
      <h3>Guess Game</h3>
      <div>
        <Button variant="contained"
          disabled={newGame}
          onClick={() => {
            setNewGame(true);
            HandleNewGame();
          }}
        >
          New Game
        </Button>
        <input
          disabled={gameState}
          onChange={(event) => setUserNum(event.target.value)}
        ></input>
        <Button
          disabled={gameState}
          onClick={() => {
            HandleGameProcess();
            Result();
          }}
        >
          Check
        </Button>
      </div>
      {result}
      <p>Attempts:{count}</p>
    </div>
  );
}
