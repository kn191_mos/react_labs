import React from "react";
import GuessGame from "./GuessGame";
import ShopCart from "./ShopCart";
import { MyCounter } from "./MyCounter";

const counters = [
  { id: 1, initial: 6, min: -5, max: 10 },
  { id: 2, initial: 5 },
  { id: 3 },
];

function App() {
  return (
    <div>
      <MyCounter inital="5" min="0" max="10"></MyCounter>
      <MyCounterList counters={counters} />
      <ShopCart />
      <GuessGame />
    </div>
  );
}

export default App;

function MyCounterList(props) {
  const myCounter = props.counters.map((counter) => (
    <MyCounter initial={counter.initial} max={counter.max} min={counter.min} />
  ));
  return <div>{myCounter}</div>;
}
