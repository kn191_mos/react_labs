import { TextareaAutosize } from "@mui/material";
import { useState } from "react";
import Button from "@mui/material/Button";
import React from "react";


  function Table() {
    const username = React.useRef();
    const email = React.useRef();
    const theme = React.useRef();
    const text = React.useRef();

    function isValidEmail(email) {
        return /\S+@\S+\.\S+/.test(email);
      }
      
  
    const [message, setMessage] = useState("");
    const [error, setError] = useState(null);
    const [errorState, setErrorState] = useState(false);
  
    const handleSubmit = () => {
      console.log("Name: " + username.current.value);
      console.log("email: " + email.current.value);
      console.log("theme: " + theme.current.value);
      console.log("text: " + text.current.value);
    };
  
    const handleChange = (event) => {
      if (!isValidEmail(event.target.value)) {
        setError("Email is invalid");
        setErrorState(true);
      } else {
        setError(null);
        setErrorState(false);
      }
  
      setMessage(event.target.value);
    };
  
    return (
      <form>
        <label>
          <input
            type="text"
            placeholder="Ім'я"
            style={{ margin: "10px", width: "250px" }}
            ref={username}
          />
        </label>
        <br />
        <label>
          <input
            id="email"
            name="email"
            value={message}
            onChange={handleChange}
            type="text"
            placeholder="E-mail*"
            required={true}
            style={{ margin: "10px", width: "250px" }}
            ref={email}
  
          />
          {error && <p style={{ color: "red", margin: "10px" }}>{error}</p>}
        </label>
        <br />
        <label>
          <input
            type="text"
            placeholder="Тема*"
            required={true}
            style={{ margin: "10px", width: "250px" }}
            ref={theme}
          />
        </label>
        <br />
        <label>
          <TextareaAutosize
            type="text"
            placeholder="Повідомлення"
            name="message"
            style={{ margin: "10px", height: "100px", width: "250px" }}
            ref={text}
          />
        </label>
        <br />
        <Button
          variant="outlined"
          maxLength={10}
          type="button"
          style={{ margin: "10px" }}
          disabled={errorState}
          onClick={handleSubmit}
        >
          Відправити
        </Button>
      </form>
    );
  }
  
  export default Table;
  