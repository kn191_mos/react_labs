import React, { useState } from "react";

export function MyCounter(props) {
  const { initial = 10, min = -10, max = 10 } = props;
  const [count, setCount] = useState(parseInt(initial));
  function changeCounter(value) {
    if (value === "increment") {
      if (count + 1 <= max) {
        setCount((prevState) => prevState + 1);
      }
    } else if (value === "decrement") {
      if (count - 1 >= min) {
        setCount((prevState) => prevState - 1);
      }
    }
  }
  return (
    <div>
      <div>Поточний рахунок {count}</div>
      <button variant="contained" onClick={() => changeCounter("increment")}>
        +
      </button>
      <button variant="contained" onClick={() => changeCounter("decrement")}>
        -
      </button>
      <button variant="contained" onClick={() => setCount(parseInt(initial))}>
        Reset
      </button>
    </div>
  );
}
