import React from "react";
import { useState } from "react";
import Button from '@mui/material/Button';



const items = [
    {
      id: 1,
      name: "Constructor LEGO",
      price: 300,
    },
    {
      id: 2,
      name: "Train Station",
      price: 200,
    },
    {
      id: 3,
      name: "How Wheels Track",
      price: 150,
    },
  ];

export default function ShopCart() {
    const [cart, setCart] = useState([]);
    const cartTotal = cart.reduce((total, { price = 0 }) => total + price, 0);
  
    const addToCart = (item) => setCart((currentCart) => [...currentCart, item]);
  
    const removeFromCart = (item) => {
      setCart((currentCart) => {
        const indexOfItemToRemove = currentCart.findIndex(
          (cartItem) => cartItem.id === item.id
        );
  
        if (indexOfItemToRemove === -1) {
          return currentCart;
        }
  
        return [
          ...currentCart.slice(0, indexOfItemToRemove),
          ...currentCart.slice(indexOfItemToRemove + 1),
        ];
      });
    };
  
    const amountOfItems = (id) => cart.filter((item) => item.id === id).length;
  
    const itemsToBuy = () =>
      items.map((item) => (
        <tr>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button type="submit" onClick={() => addToCart(item)}>
              +
            </button>
          </td>
          <td>{amountOfItems(item.id)}</td>
          <td>
            <button type="submit" onClick={() => removeFromCart(item)}>
              -
            </button>
          </td>
        </tr>
      ));
  
    return (
      <table>
        <h3>Shop</h3>
        <tr>
          <th>Name</th>
          <th>Price</th>
          &nbsp;
          <th>Quantity</th>
        </tr>
        {itemsToBuy()}
        <div>Total: {cartTotal}</div>
      </table>
    );
  }