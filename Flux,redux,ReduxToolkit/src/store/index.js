import counterReducer, { addUser, addUsers, removeUser } from "./reducers/counterReducer";
import thunk from "redux-thunk";
import { configureStore } from "@reduxjs/toolkit";

export const store = configureStore({
    reducer: counterReducer,
    middleware: [thunk]
})
// export const store = createStore(counterSlice, applyMiddleware(thunk));