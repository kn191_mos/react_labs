import { createSlice } from "@reduxjs/toolkit";

const defaultState = {
  counter: 20,
  lesson: {
    lection: 10,
    topic: "Redux Toolkit",
  },
  users: [],
};

const counterSlice = createSlice({
  name: "counters",
  reducers: {
    increaseCounter(state, action) {
      return { ...state, counter: state.counter + Number(action.payload) };
    },
    decreaseCounter(state, action) {
      return { ...state, counter: state.counter - Number(action.payload) };
    },
    addUser(state, action) {
      return { ...state, users: [...state.users, action.payload] };
    },
    addUsers(state, action) {
      return { ...state, users: [...state.users, ...action.payload] };
    },
    removeUser(state, action) {
      return {
        ...state,
        users: state.users.filter((user) => user.id !== action.payload),
      };
    },
  },
});

// const counterReducer = (state = defaultState, action) => {
//     switch (action.type) {
//         case "INCREASE_COUNTER":
//             return {...state, counter: state.counter + Number(action.payload)}
//         case "DECREASE_COUNTER":
//             return {...state, counter: state.counter - Number(action.payload)}
//         case "ADD_USER":
//             return {...state, users: [...state.users, action.payload]}
//         case "ADD_USERS":
//             return {...state, users: [...state.users, ...action.payload]}
//         case "REMOVE_USER":
//             return {...state, users: state.users.filter(user => user.id !== action.payload)}
//         default:
//             return state;
//     }
// }

export const {addUser, addUsers, removeUser} = counterSlice.actions;
export default counterSlice.reducer;
